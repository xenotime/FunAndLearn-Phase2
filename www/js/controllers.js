angular.module('starter.controllers', [])

    .controller('DashCtrl', function($scope,$rootScope,surveys) {

        surveys.all().
          success(function(data, status, headers, config) {
              if(status == 200) {
                  $rootScope.surveys = data;
              }
          });
      $scope.remove = function(survey) {
          $rootScope.surveys.splice($rootScope.surveys.indexOf(survey), 1);
      }
    })

    .controller('SurveyDetailCtrl', function($scope, $rootScope, $stateParams, surveys) {
        for (var i = 0; i < $rootScope.surveys.length; i++) {
            if ($rootScope.surveys[i].id === parseInt($stateParams.surveyId)) {
                $scope.survey =  $rootScope.surveys[i];
                $rootScope.survey = $scope.survey;
            }
        }
    })

    .controller('QuestionCtrl', function($scope, $rootScope, $stateParams, questions) {
        $scope.currentQuestion = 0;
        $scope.questions = [];
        questions.get().
            success(function(data, status, headers, config) {
                if(status == 200) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].survey === parseInt($stateParams.surveyId)) {
                            $scope.questions.push(data[i]);
                        }
                    }

                }
            });

        $scope.next = function() {
            $scope.currentQuestion++;
        }
        $scope.back = function() {
            $scope.currentQuestion--;
        }
    })
