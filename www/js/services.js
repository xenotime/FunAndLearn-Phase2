angular.module('starter.services', [])

    .factory('surveys', function($http) {
      return {
        all: function() {
          return $http.get('http://www.mocky.io/v2/55b481e51c5bf12415c900b0');
        }
      };
    })
    .factory('questions', function($http) {
      return {
        get: function() {
          return $http.get('http://www.mocky.io/v2/55b482351c5bf12f15c900b1');
        }
      };
    });
